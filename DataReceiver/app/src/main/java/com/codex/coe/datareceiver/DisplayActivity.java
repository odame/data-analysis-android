package com.codex.coe.datareceiver;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DisplayActivity extends AppCompatActivity {

    private AVLoadingIndicatorView animationView;
    private TextView allDataTextView;
    private TextView sentDataTextView;
    private TextView failedDataTextView;
    private TextView scheduledDataTextView;
    private Button resendNowButton;
    private ProgressDialog progressDialog;

    private final Object dbAccessLock = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        setupUI();
    }

    private void setupUI(){
        animationView = (AVLoadingIndicatorView)findViewById(R.id.animation);
        allDataTextView = (TextView)findViewById(R.id.all_data_text_view);
        sentDataTextView = (TextView)findViewById(R.id.sent_data_text_view);
        failedDataTextView = (TextView)findViewById(R.id.failed_data_text_view);
        scheduledDataTextView = (TextView)findViewById(R.id.scheduled_data_text_view);
        resendNowButton = (Button)findViewById(R.id.button_resend_now);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(this, R.drawable.indeterminate_drawable));
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        animationView.smoothToShow();

        getStatistics();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(this.dbUpdateBroadCastReceiver, new IntentFilter(IntentConstants.DB_UPDATE_INTENT_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.dbUpdateBroadCastReceiver);
    }

    private synchronized void getStatistics(){
        int failedMessagesCount = 0;
        int sentDataCount = 0;
        int allDataCount = 0;

        try {
            DB snappydb = DBFactory.open(getApplicationContext(), IntentConstants.APP_DATABASE_NAME);

            try{
                sentDataCount = snappydb.getInt(IntentConstants.SENT_DATA_COUNT_PK);
            }
            catch (SnappydbException e) {}
            try{
                allDataCount = snappydb.getInt(IntentConstants.ALL_RECEIVED_DATA_COUNT_PK);
            }
            catch (SnappydbException e) {}
            try{
                failedMessagesCount = snappydb.countKeys(IntentConstants.FAILED_MESSAGE_PK);
            }
            catch (SnappydbException e) {}

            snappydb.close();
        } catch (SnappydbException e) {}

        allDataTextView.setText(Integer.toString(allDataCount));
        sentDataTextView.setText(Integer.toString(sentDataCount));
        failedDataTextView.setText(Integer.toString(failedMessagesCount));
        scheduledDataTextView.setText(Integer.toString(failedMessagesCount));
        resendNowButton.setVisibility(failedMessagesCount == 0? View.GONE:View.VISIBLE);
    }

    private BroadcastReceiver dbUpdateBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getStatistics();
        }
    };

    //region SYNC DATA
    private void syncData(){
        //we remove all the failed messages from the database and send them individually
        //if a message fails, it will be saved
        List<String> failedMessages = new ArrayList<>();
        synchronized (dbAccessLock){
            try {
                DB snappydb = DBFactory.open(this, IntentConstants.APP_DATABASE_NAME);
                String[] keys = snappydb.findKeys(IntentConstants.FAILED_MESSAGE_PK);
                int failedMessagesCount = keys.length;
                for(int i = 0; i< failedMessagesCount; i++){
                    //we remove all failed messages from the database
                    //and add them to the string array
                    failedMessages.add(snappydb.get(keys[i]));
                    snappydb.del(keys[i]);
                }
                snappydb.close();
            } catch (SnappydbException e) {}
        }
        //there are now, no failed messages in the database
        getStatistics(); //so we update the UI to reflect the changes

        //we retry sending the messages
        for(String message: failedMessages){
            sendDataToServerAsync(message);
        }
    }

    private void sendDataToServerAsync(final String data){
        synchronized (dbAccessLock){
            try {
                DB snappydb = DBFactory.open(getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
                int sentDataCount = 0;
                try{
                    sentDataCount = snappydb.getInt(IntentConstants.SENT_DATA_COUNT_PK);
                }
                catch (SnappydbException ee){}
                snappydb.putInt(IntentConstants.SENT_DATA_COUNT_PK, sentDataCount + 1);
                snappydb.close();
            } catch (SnappydbException e) {}
        }
        notifyDBUpdated();


//        Thread thread = new Thread(){
//            @Override
//            public void run() {
//                Retrofit retrofit = new Retrofit.Builder().baseUrl(IntentConstants.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
//                APIInterface apiInterface = retrofit.create(APIInterface.class);
//                Call<APIResponse> call = apiInterface.postData(data);
//                try {
//                    Response<APIResponse> response = call.execute();
//
//                    if(response.isSuccessful()){
//                        //we increment the number of successfully sent data
//                        synchronized (dbAccessLock){
//                            try {
//                                DB snappydb = DBFactory.open(getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
//                                int sentDataCount = 0;
//                                try{
//                                    sentDataCount = snappydb.getInt(IntentConstants.SENT_DATA_COUNT_PK);
//                                }
//                                catch (SnappydbException ee){}
//                                snappydb.putInt(IntentConstants.SENT_DATA_COUNT_PK, sentDataCount + 1);
//                                snappydb.close();
//                            } catch (SnappydbException e) {}
//                        }
//                        notifyDBUpdated();
//                    }
//                    //we save the data locally, and send later if the request fails
//                    else{
//                        saveDataLocallyAsync(data);
//                    }
//                }
//                //the exceptions below may be due to bad internet connection
//                catch(IOException ioExc){
//                    saveDataLocallyAsync(data);
//                }
//                catch (RuntimeException rnExc){
//                    saveDataLocallyAsync(data);
//                }
//            }
//        };
//        thread.start();
    }

//    private synchronized void saveDataLocallyAsync(final String data){
//        synchronized (dbAccessLock){
//            try {
//                DB snappydb = DBFactory.open(getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
//                final int failedMessagesCount = snappydb.countKeys(IntentConstants.FAILED_MESSAGE_PK);
//                snappydb.put(IntentConstants.FAILED_MESSAGE_PK + Integer.toString(failedMessagesCount + 1), data);
//                snappydb.close();
//            } catch (SnappydbException e) {}
//            notifyDBUpdated();
//        }
//    }

    private synchronized void notifyDBUpdated(){
        Intent intent = new Intent(IntentConstants.DB_UPDATE_INTENT_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void onResendNowClick(View view){
        progressDialog.show();
        syncData();
        progressDialog.hide();
    }
    //endregion
}
