package com.codex.coe.datareceiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;

import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppBroadcastReceiver extends BroadcastReceiver {
    private static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String MESSAGE_WRAP_CODE = "|";
    private static final String FIELD_DELIMITER = "~";
    private static final String MESSAGES_DELIMITER = "`";

    //object for locking database access to avoid race conditions
    private final SEMAPHORE dbAccessLock = new SEMAPHORE();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(ACTION_SMS_RECEIVED)){
            String message = "";
            String senderPhoneNumber; //// TODO: 2016-08-20 Save the list of expected phone numbers
            SmsMessage[] msgs = getMessagesFromIntent(intent);
            senderPhoneNumber = msgs[0].getOriginatingAddress();

            if (msgs != null) {
                for (int i = 0; i < msgs.length; i++) {

                    message += msgs[i].getMessageBody();
                    message += "\n";
                }
            }
            message = message.substring(0,message.length()-1);
            boolean startWrapped = message.substring(0,1).equals(MESSAGE_WRAP_CODE);
            boolean endWrapped = message.substring(message.length() - 1).equals(MESSAGE_WRAP_CODE);
            if( startWrapped && endWrapped){
                //we increment the number of received data in the database
                synchronized (dbAccessLock){
                    try {
                        DB snappyDb = DBFactory.open(context.getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
                        int allDataCount = 0;
                        try{
                            allDataCount = snappyDb.getInt(IntentConstants.ALL_RECEIVED_DATA_COUNT_PK);
                        }
                        catch(SnappydbException ee){}
                        snappyDb.putInt(IntentConstants.ALL_RECEIVED_DATA_COUNT_PK, allDataCount + 1);
                        snappyDb.close();
                    }
                    catch (SnappydbException e) {}
                }
//                notifyDBUpdated(context);

                //we send the data to the server on a different thread
                sendDataToServerAsync(message, context);
            }
        }
        else if(action.equals(ConnectivityManager.CONNECTIVITY_ACTION)){
            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();

            //if we are online, we perform a sync
            if(netInfo != null && netInfo.isConnected()){
                syncData(context);
            }
        }
    }

    //region DATA SYNC
    private void syncData(Context context){
        //we remove all the failed messages from the database and send them individually
        //if a message fails, it will be saved
        List<String> failedMessages = new ArrayList<>();
        synchronized (dbAccessLock){
            try {
                DB snappydb = DBFactory.open(context.getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
                String[] keys = snappydb.findKeys(IntentConstants.FAILED_MESSAGE_PK);
                int failedMessagesCount = keys.length;
                for(int i = 0; i< failedMessagesCount; i++){
                    //we remove all failed messages from the database
                    //and add them to the string array
                    failedMessages.add(snappydb.get(keys[i]));
                    snappydb.del(keys[i]);
                }
                snappydb.close();
            } catch (SnappydbException e) {}
        }
        //we retry sending the messages
        for(String message: failedMessages){
            sendDataToServerAsync(message, context);
        }
    }
    //endregion

    public static SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] pduObjs = new byte[messages.length][];

        for (int i = 0; i < messages.length; i++) {
            pduObjs[i] = (byte[]) messages[i];
        }
        byte[][] pdus = new byte[pduObjs.length][];
        int pduCount = pdus.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++) {
            pdus[i] = pduObjs[i];
            msgs[i] = SmsMessage.createFromPdu(pdus[i]);
        }
        return msgs;
    }

    private void sendDataToServerAsync(final String data, final Context context){
        synchronized (dbAccessLock){
            try {
                DB snappydb = DBFactory.open(context.getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
                int sentDataCount = 0;
                try{
                    sentDataCount = snappydb.getInt(IntentConstants.SENT_DATA_COUNT_PK);
                }
                catch (SnappydbException ee){}
                snappydb.putInt(IntentConstants.SENT_DATA_COUNT_PK, sentDataCount + 1);
                snappydb.close();
            } catch (SnappydbException e) {}
            notifyDBUpdated(context);
        }

//        Thread thread = new Thread(){
//            @Override
//            public void run() {
//                Retrofit retrofit = new Retrofit.Builder().baseUrl(IntentConstants.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
//                APIInterface apiInterface = retrofit.create(APIInterface.class);
//                Call<APIResponse> call = apiInterface.postData(data);
//                try {
//                    Response<APIResponse> response = call.execute();
//
//                    if(response.isSuccessful()){
//                        //we increment the number of successfully sent data
//                        synchronized (dbAccessLock){
//                            try {
//                                DB snappydb = DBFactory.open(context.getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
//                                int sentDataCount = 0;
//                                try{
//                                   sentDataCount = snappydb.getInt(IntentConstants.SENT_DATA_COUNT_PK);
//                                }
//                                catch (SnappydbException ee){}
//                                snappydb.putInt(IntentConstants.SENT_DATA_COUNT_PK, sentDataCount + 1);
//                                snappydb.close();
//                            } catch (SnappydbException e) {}
//                            notifyDBUpdated(context);
//                        }
//                    }
//                    //we save the data locally, and send later if the request fails
//                    else{
//                        saveDataLocallyAsync(data, context);
//                    }
//                }
//                //the exceptions below may be due to bad internet connection
//                catch(IOException ioExc){
//                    saveDataLocallyAsync(data, context);
//                }
//                catch (RuntimeException rnExc){
//                    saveDataLocallyAsync(data, context);
//                }
//            }
//        };
//        thread.start();
    }

//    private synchronized void saveDataLocallyAsync(final String data, final Context context){
//        synchronized (dbAccessLock){
//            try {
//                DB snappydb = DBFactory.open(context.getApplicationContext(), IntentConstants.APP_DATABASE_NAME);
//                final int failedMessagesCount = snappydb.countKeys(IntentConstants.FAILED_MESSAGE_PK);
//                snappydb.put(IntentConstants.FAILED_MESSAGE_PK + Integer.toString(failedMessagesCount + 1), data);
//                snappydb.close();
//            } catch (SnappydbException e) {}
//            notifyDBUpdated(context);
//        }
//    }

    //send out a broadcast to notify that the database has been updated with some valued
    //if an instance of DisplayActivity is alive, it will receive the broadcast and update
    //its UI as such
    private synchronized void notifyDBUpdated( Context context){
        Intent intent = new Intent(IntentConstants.DB_UPDATE_INTENT_ACTION);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public class SEMAPHORE{
    }
}
