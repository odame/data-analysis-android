package com.codex.coe.datareceiver;

public class IntentConstants {
    public static final String BASE_URL = "https://filariasis.herokuapp.com/";

    public static final String APP_DATABASE_NAME = "DATA_BROADCAST_DATABASE";
    public static final String FAILED_MESSAGE_PK = "failed_messages ";
    public static final String ALL_RECEIVED_DATA_COUNT_PK = "all_received_data_count";
    public static final String SENT_DATA_COUNT_PK = "all_sent_data_count";

    public static final String DB_UPDATE_INTENT_ACTION = "DB_UPDATE_INTENT_ACTION";
}
