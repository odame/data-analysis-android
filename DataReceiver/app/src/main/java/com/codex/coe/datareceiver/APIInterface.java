package com.codex.coe.datareceiver;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIInterface {
    @FormUrlEncoded
    @POST("data/")
    Call<APIResponse> postData(@Field("message_body") String messageBody);
}
