package com.codex.coe.datareceiver;


import com.google.gson.annotations.SerializedName;

public class APIResponse {
    @SerializedName("message")
    String message;

    public String getMessage(){
        return this.message;
    }
}
