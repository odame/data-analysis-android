package com.codex.coe.databroadcast;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String MESSAGE_WRAP_CODE = "|";
    private static final String FIELD_DELIMITER = "~";
    private static final String MESSAGES_DELIMITER = "`";

    private static final String DATABASE_NAME = "DATABASE_NAME_167";
    private static final String FAILED_MESSAGE_PK = "failed_messages ";
    private static final String PHONE_NUMBER_PK = "PHONE_NUMBER";

    private static final String SMS_SENT_STATUS_INTENT = "SMS_SENT_STATUS_INTENT";

    private MultiAutoCompleteTextView nameTextView;
    private RadioGroup genderRadioGroup;
    //private Button dateOfBirthButton;
    private EditText phoneNumberEditText;
    private EditText houseNumberEditText;
    private ProgressDialog progressDialog;
    private Menu menu;

    private Date dateOfBirth;
    private SEMAPHORE dbAccessSemaphore = new SEMAPHORE();
    private String phoneNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the phone number from the database
        try {
            phoneNumber = "0501377957";
            DB snappyDB = DBFactory.open(getApplicationContext(), DATABASE_NAME);
            try {
                phoneNumber = snappyDB.get(PHONE_NUMBER_PK);
            } catch (SnappydbException excc) {
            }
            snappyDB.close();
        }
        catch (SnappydbException exc) {}

        this.setupUI();
        //027 892 5201
        this.registerReceiver(smsSentReceiver, new IntentFilter(SMS_SENT_STATUS_INTENT));
    }

    private void setupUI() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nameTextView = (MultiAutoCompleteTextView) findViewById(R.id.edit_text_name);
        genderRadioGroup = (RadioGroup) findViewById(R.id.radio_group_gender);
        //dateOfBirthButton = (Button) findViewById(R.id.button_date_of_birth);
        phoneNumberEditText = (EditText) findViewById(R.id.edit_text_phone_number);
        houseNumberEditText = (EditText) findViewById(R.id.edit_text_house_number);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.indeterminate_drawable));
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;

        //update the menu to reflect the failed messages
        synchronized (dbAccessSemaphore) {
            try {
                DB snappydb = DBFactory.open(this, DATABASE_NAME);
                int failedMessagesCount = snappydb.countKeys(FAILED_MESSAGE_PK);
                snappydb.close();
                setFailedMessagesCount(failedMessagesCount); //set in the menu, the number of failed messages
            } catch (SnappydbException e) {
            }
        }

        menu.findItem(R.id.action_sync_now).getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                syncMessages();
            }
        });

        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.action_phone_number:
//                Dialog dialog = getChangeUsernameDialog();
//                dialog.show();
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void syncMessages() {
        Toast.makeText(MainActivity.this, "Sync has started", Toast.LENGTH_SHORT).show();
        progressDialog.show();

        synchronized (dbAccessSemaphore) {
            try {
                DB snappydb = DBFactory.open(this, DATABASE_NAME);
                String[] keys = snappydb.findKeys(FAILED_MESSAGE_PK);
                int failedMessagesCount = keys.length;
                String[] failedMessages = new String[failedMessagesCount];
                for (int i = 0; i < failedMessagesCount; i++) {
                    //we remove all failed messages from the database
                    //and add them to the string array
                    failedMessages[i] = snappydb.get(keys[i]);
                    snappydb.del(keys[i]);
                }
                snappydb.close();
                //there are now no saved failed messages to sync
                setFailedMessagesCount(0); //set in the menu, the number of failed messages

                //we retry sending the messages
                for (String message : failedMessages) {
                    sendDataSMS(message, true);
                }
            } catch (SnappydbException e) {
            }
        }

        progressDialog.dismiss();
    }

    //remove data from all the fields
    private void clearFields() {
        nameTextView.setText("");
        genderRadioGroup.clearCheck();
        this.dateOfBirth = null;
        //dateOfBirthButton.setText("(CLICK TO SET)");
        phoneNumberEditText.setText("");
        houseNumberEditText.setText("");

        nameTextView.requestFocus();
    }

    public void onSendButtonClick(View view) {
        if (validateData()) {
            progressDialog.show();

            String message = this.generateSMSString();
            sendDataSMS(message, false);
        }
    }

    public void onDateOfBirthClick(View view) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("datePicker");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.commit();
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.dateSelectedListener = this.onDateOfBirthSelectedListener;
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private Dialog getChangeUsernameDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.phone_number_setting_layout, null);

        final EditText phoneNumberEditText = (EditText) dialog_layout.findViewById(R.id.phone_number);
        phoneNumberEditText.setHint(phoneNumber);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialog_layout).setTitle("Change Username").setCancelable(true)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // dismisses the dialog
                    }
                })
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // save the new name
                        final String newPhoneNumber = phoneNumberEditText.getText().toString();
                        if (newPhoneNumber.equals("")) {
                            Toast.makeText(MainActivity.this, "Please enter a valid phone number", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //get the phone number from the database
                        try {
                            DB snappyDB = DBFactory.open(getApplicationContext(), DATABASE_NAME);
                            snappyDB.put(PHONE_NUMBER_PK, newPhoneNumber);
                            snappyDB.close();
                        } catch (SnappydbException exc) {
                            Toast.makeText(MainActivity.this, "Could not set phone number\nPlease try again", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        phoneNumber = newPhoneNumber;
                        Toast.makeText(MainActivity.this, "Phone number has been set successfully", Toast.LENGTH_SHORT).show();
                    }
                });

        final AlertDialog dialog = builder.create();

        return dialog;
    }

    //region SMS stuff
    private String generateSMSString() {
        List<String> fields = new ArrayList<>();
        fields.add(new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Calendar.getInstance().getTime()));//current time
        fields.add(nameTextView.getText().toString().trim()); //name
        fields.add(genderRadioGroup.getCheckedRadioButtonId() == R.id.radio_female ? "1" : "2"); //gender. 1-female, 2-male
        fields.add(new java.text.SimpleDateFormat("dd-MM-yyyy").format(dateOfBirth));//date of birth
        fields.add(phoneNumberEditText.getText().toString().trim()); //phone mnumber
        fields.add(houseNumberEditText.getText().toString().trim()); //house number
        String message = "";
        int i = 0;
        for (String field : fields) {
            if (++i == 1) //we don't add the delimiter for the first time
                message += field;
            else
                message += FIELD_DELIMITER + field;
        }

        message = MESSAGE_WRAP_CODE + message + MESSAGE_WRAP_CODE;
        //at the end, message = |{message}|
        return message;
    }

    private void sendDataSMS(final String message, boolean isSync) {
        SmsManager smsManager = SmsManager.getDefault();

        ArrayList<String> parts = smsManager.divideMessage(message);
        int numParts = parts.size();
        ArrayList<PendingIntent> sentIntents = new ArrayList<>();
        for (int i = 0; i < numParts; i++) {
            Intent sentIntent = new Intent(SMS_SENT_STATUS_INTENT);
            sentIntent.putExtra("MESSAGE", message);
            sentIntent.putExtra("IS_SYNC", isSync);
            sentIntents.add(PendingIntent.getBroadcast(this, 0, sentIntent, PendingIntent.FLAG_ONE_SHOT));
        }

        smsManager.sendMultipartTextMessage(phoneNumber, null, parts, sentIntents, null);

        MainActivity.this.clearFields();
    }

    private BroadcastReceiver smsSentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    if (!intent.getBooleanExtra("IS_SYNC", false)) {
                        Toast.makeText(MainActivity.this, "Data has been sent successfully", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default: //if there was an error while sending the sms
                    //we save the message to send later
                    synchronized (dbAccessSemaphore) {
                        try {
                            DB snappydb = DBFactory.open(context, DATABASE_NAME);
                            int failedMessagesCount = snappydb.countKeys(FAILED_MESSAGE_PK);
                            snappydb.put(FAILED_MESSAGE_PK + Integer.toString(++failedMessagesCount), intent.getStringExtra("MESSAGE"));
                            snappydb.close();
                            setFailedMessagesCount(failedMessagesCount); //set in the menu, the number of failed messages
                        } catch (SnappydbException e) {
                        }
                    }
                    if (!intent.getBooleanExtra("IS_SYNC", false)) {
                        Toast.makeText(MainActivity.this, "Could not send data.\nPlease SYNC to try again", Toast.LENGTH_SHORT).show();
                    }
            }

//            MainActivity.this.unregisterSentMessagePendingIntent();
            MainActivity.this.progressDialog.dismiss(); //hide the progress dialog
        }
    };

    private synchronized void setFailedMessagesCount(int failedMessagesCount) {
        MenuItem syncNowMenuItem = this.menu.findItem(R.id.action_sync_now);
        if (failedMessagesCount == 0) {
            syncNowMenuItem.setVisible(false);
            syncNowMenuItem.setEnabled(false);
        } else {
            syncNowMenuItem.setVisible(true);
            syncNowMenuItem.setEnabled(true);
            TextView countTextView = (TextView) syncNowMenuItem.getActionView().findViewById(R.id.text_view_count);
            countTextView.setText(Integer.toString(failedMessagesCount));
        }
    }

    private void unregisterSentMessagePendingIntent() {
        this.unregisterReceiver(smsSentReceiver);
    }

    private boolean validateData() {
        boolean hasErrors = false;
        //validate the name  field
        if (nameTextView.getText().toString().trim().length() == 0) {
            nameTextView.setError("Please provide name");
            hasErrors = true;
        }

        //validate the gender field
        if (genderRadioGroup.getCheckedRadioButtonId() == -1) {//if no radio button is checked, the return value is -1
            hasErrors = true;
        }

        //validate the phone number  field
        if (phoneNumberEditText.getText().toString().trim().length() == 0) {
            phoneNumberEditText.setError("Please provide phone number");
            hasErrors = true;
        } else if (phoneNumberEditText.getText().toString().length() != 10) {
            phoneNumberEditText.setError("Please provide a valid phone number");
            hasErrors = true;
        }
        //// TODO: 2016-08-19 Validate phone mnumber with regex or google library

        //validate house number
        if (houseNumberEditText.getText().toString().trim().length() == 0) {
            houseNumberEditText.setError("Please provide house number");
            hasErrors = true;
        }

        //validate date of birth
        if (dateOfBirth == null) {
            hasErrors = true;
        }

        if (hasErrors) {
            Toast.makeText(MainActivity.this, "Some fields have errors.\nPlease check them and try again", Toast.LENGTH_SHORT).show();
        }
        return !hasErrors;
    }
    //endregion

    private DatePickerFragment.DateSelectedListener onDateOfBirthSelectedListener = new DatePickerFragment.DateSelectedListener() {
        @Override
        public void onDateSelected(Date selectedDate) {
            if (selectedDate != null) {
                String dateInString = new java.text.SimpleDateFormat("EEEE, dd-MM-yyyy").format(selectedDate);
                //dateOfBirthButton.setText(dateInString);
                MainActivity.this.dateOfBirth = selectedDate;
            }
        }
    };

    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        private DateSelectedListener dateSelectedListener;
        private boolean isCancelled = false;
        private Date selectedDate = null;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                selectedDate = formatter.parse(String.format("%d-%d-%d", i, i1, i2));
            } catch (ParseException e) {
                selectedDate = null;
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            this.isCancelled = true;
            super.onCancel(dialog);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            super.onDismiss(dialog);
            if (!this.isCancelled) {
                dateSelectedListener.onDateSelected(selectedDate);
            }
        }

        public interface DateSelectedListener {
            void onDateSelected(Date selectedDate);
        }
    }

    private class SEMAPHORE {
        private boolean busy = false; //inidicates that an SMS is currently being sent
    }
}
